const WebSocket = require('ws');
const wss = new WebSocket.Server({ port: 7777 });
// endpoint: ws://localhost:7777

const clients = [];
// const clients = [ { client, id },  ];
const clientIds = [];

wss.on('connection', function connection(ws) {
  console.log('made connection');

  const client = { ws, id: null };
  clients.push(client);

  let count = 0;

  ws.on('message', function incoming(message) {

    // Grab the id attached to the sender
    // const id = message.id;

    // Set the ws's id property to the id sent to us
    // from the message
    console.log('message type', typeof message);
    const { id } = JSON.parse(message);
    client.id = id;

    // Append the id to our list of clientIds if it doesn't exist yet
    // if(!clientIds.includes(id)) {
    //   clientIds.push(id);
    // }



    console.log('received message', message);
    if (count === 10) {
      console.log('terminating socket');
      ws.terminate();
    } else {
      console.log('sending back message', message);

      console.log('clients length', clients.length);
      console.log('clients (ids)', clients.map(client => client.id));

      // Broadcast the message to all clients that match
      // the id of the sender
      clients
        .filter(client => client.id === id)
        .forEach(client => {
          client.ws.send(message);
        });


      // ws.send(message);
      // clients.forEach(ws => {
      //   ws.send(message);
      // });

      count++;
      console.log('message sent, count', count);
    }
  });
});